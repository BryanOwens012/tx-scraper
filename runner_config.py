### Config settings for ./scraper.py
# Do not change the variable names

origin_address = '38p77ZLLHTzEd8TdMRttteD6DuFmxkyhY3'
addresses_dir = 'addresses'
refresh = False # Refresh data if already downloaded (cached) in the addresses_dir directory

maxtime = 15 # In seconds
maxdepth = 100
maxsize = 1000 # Queue size limit

# Timestamp truncation
# Either None, or a two-element list [begin, end] in terms of timestamps (epoch time, in seconds)
# Use this tool to convert human time to timestamps: https://www.epochconverter.com/
timestamp_range = None

# Row truncation always happens after timestamp truncation, if any
row_range = [0, 1000] # Either None, or a two-element list [begin, end] in terms of rows


