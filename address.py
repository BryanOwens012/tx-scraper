#!/usr/bin/env python3

'''
Stores the data associated with each address
'''


# BeautifulSoup help from:
# https://www.learndatasci.com/tutorials/ultimate-guide-web-scraping-w-python-requests-and-beautifulsoup/

from functools import total_ordering
from bs4 import BeautifulSoup # parse HTML
import os
from pathlib import Path
import requests
import datetime

from payment import Payment
from address_profile import AddressProfile
from scraper import scrape_all_txns

# Decorator facilitates the comparison operators in case we decide to sort Addresses in the future
@total_ordering
class Address:
    '''
    Stores the attributes of an address plus the payments the address is involved in

    Attributes
    ----------
    address : str
        The address
    crypto : str
        The abbreviation of the crypto, e.g. 'BTC'
    dir : str
        The name of the directory in which we store the downloaded pages (HTML) of the scraped addresses
    path : Path
        The path of this address's downloaded page (HTML)
    url : str
        The URL of the address's page on Blockchain.com
    last_updated : int
        The UNIX timestamp (in seconds since the epoch) when we last refreshed this address's downloaded page (HTML)
    debug : bool
        Whether to print debug messages
    payments: List[Payment]
        The list of payments that this address is involved in
    connected_addresses : List[str]
        The list of addresses that this address had a payment with
    address_profile : AddressProfile
        The object that stores the summarized transaction data (like # of transactions) for this address

    Methods
    ----------
    @staticmethod _file_exists : Path --> bool
        Returns whether the file exists
    @staticmethod _save_html : str, Path --> str
        Saves the HTML to the path, and returns the HTML again
    @staticmethod _read_html : Path --> str
        Reads the file at the path
    log : str --> None
        Prints the message if self.debug is True
    download : bool --> None
        If False, then download only if the file isn't already downloaded
        If True, then download regardless
    refresh_download : None --> None
        Download the file, overwriting it if it already exists
        Update the self.last_updated timestamp for bookkeeping
    scrape_address_txns : None --> None
        Scrape all transactions on the address's page, and store their payments into self.payments
    make_address_profile : None --> None
        Store the address's summary data into self.address_profile

    '''

    def __init__(self, address, crypto = 'BTC', dir = 'addresses', path = None):
        self.address = address
        self.crypto = crypto.upper()

        self.dir = dir
        self.path = path
        if self.path is None:
            self.path = Path(os.getcwd()) / self.dir / (self.crypto.lower() + '-' + self.address + '.html')
        self.url = 'https://www.blockchain.com/' + self.crypto.lower() + '/address/' + self.address
        self.last_updated = None # epoch time in seconds

        self.debug = True

        self.payments = []
        self.connected_addresses = []
        self.address_profile = None

    def __str__(self):
        return self.__class__.__name__ + '(' + self.address + ' (' + self.crypto + ') at ' + str(self.path) + ')'

    def __eq__(self, other):
        return (self.address, self.crypto) == (other.address, other.crypto)

    def __lt__(self, other):
        return (self.crypto, self.address) < (self.crypto, self.address)

    @staticmethod
    def _file_exists(path):
        if isinstance(path, Path):
            return path.exists()
        return os.path.exists(path)

    @staticmethod
    def _save_html(html, path):
        with open(str(path), 'wb+') as f:
            f.write(html)
        return html

    @staticmethod
    def _read_html(path):
        with open(str(path), 'r') as f:
            return f.read()

    def log(self, content):
        if self.debug:
            if isinstance(content, list):
                print(content[0], end = content[1])
                return
            print(content)

    def download(self, refresh = False):
        if not refresh and self._file_exists(self.path):
            return
        self.refresh_download()

    def refresh_download(self):
        r = requests.get(self.url)
        self._save_html(r.content, self.path)

        self.last_updated = datetime.datetime.timestamp(datetime.datetime.now())
        self.log(f'Downloaded {self.path}')

    def scrape_address_txns(self):
        self.log([f'Processing {self.address}...', ' '])
        self.n_txs = 0
        self.n_payments = 0
        self.volume = 0
        if not self._file_exists(self.path):
            self.download()

        html = self._read_html(self.path)
        self.payments, self.connected_addresses, self.volume, self.n_txns, self.n_payments = scrape_all_txns(html, self.address, self.crypto)

        self.log(f'{self.n_txns} txs containing {self.n_payments} payments totalling {self.volume} {self.crypto}')

    def make_address_profile(self):
        if not self._file_exists(self.path):
            self.download()
        self.address_profile = AddressProfile(self.address, self.crypto)
        soup = BeautifulSoup(self._read_html(self.path), 'html.parser')
        # self.address_profile.hash160 = soup.findAll('a', {'class': 'mobile-f12'})[1].text

        self.address_profile.num_transactions, self.address_profile.total_received, self.address_profile.total_sent, self.address_profile.final_balance  \
            = [span.getText() for span in soup.findAll('span', {'class': 'sc-1ryi78w-0 cILyoi sc-16b9dsl-1 ZwupP u3ufsr-0 eQTRKC'})[1:5]]