#!/usr/bin/env python3

'''
(1) Scrapes the HTML for each address
(2) Packages the payments data into Payment objects
(3) Accumulates a list of payment counterparties, to be added to the BFS queue
(4) Does some summation statistics

Functions
---------
str_to_epoch : str, str --> int
    Converts a formatted time string into a UNIX epoch timestamp

scrape_one_txn : BeautifulSoup(HTML), str, str --> List[Payment], List[str], float
    Scrape one txn
    Return the list of payments in the txn, the list of counterparty addresses to add to the BFS queue, and the volume

scrape_all_txns : BeautifulSoup(HTML), str, str --> List[Payment], List[str], float, int, int
    A wrapper for scrape_one_txn()
    Calls scrape_one_txn() on each txn, runs some statistics
    Returns everything that the scrape_one_txn() calls returned
'''

from bs4 import BeautifulSoup # parse HTML
from payment import Payment
import datetime

def str_to_epoch(formatted_time, format = '%Y-%m-%d %H:%M'):
    # https://stackoverflow.com/a/30468488
    EPOCH = datetime.datetime(1970, 1, 1)
    return (datetime.datetime.strptime(formatted_time, format) - EPOCH).total_seconds()

def scrape_one_txn(txdiv, current_addr, crypto):
    soup = BeautifulSoup(str(txdiv), 'html.parser')
    rows = soup.findAll('div', {'class': 'odi4cq-0 gVJVZs'})

    # Timestamp is in row[0]
    timestamp = str_to_epoch(rows[1].find('span', {
        'class': 'sc-1ryi78w-0 cILyoi sc-16b9dsl-1 ZwupP u3ufsr-0 eQTRKC'}).getText())  # epoch time in seconds

    # Payments are in row[1]
    from_col, to_col = rows[2], rows[3]

    # Count the number of from_addresses and to_addresses
    from_addr_divs = from_col.findAll('div', {'class': 'ge5wha-0 gMWvxK'})
    to_addr_divs = to_col.findAll('div', {'class': 'sc-19pxzmk-0 dVzTcW'})

    # Process the from_addresses
    from_addr_with_quantities = []
    for from_addr_div in from_addr_divs:
        quantity = float(from_addr_div.find('div', {'class': 'ge5wha-1 esnCUU'}).find('span', {
            'class': 'sc-1ryi78w-0 cILyoi sc-16b9dsl-1 ZwupP u3ufsr-0 eQTRKC'}).getText().split(' ')[0])
        entries = from_addr_div.findAll('span', {'class': 'sc-1ryi78w-0 cILyoi sc-16b9dsl-1 ZwupP u3ufsr-0 eQTRKC'})

        if len(entries) == 2:
            # If this is the current address (and so there's no link)
            quantity = float(entries[1].getText().split(' ')[0])
            from_addr = current_addr
        else:
            # If this is a different address (and so it's a link)
            from_addr = from_addr_div.find('a', {
                'class': 'sc-1r996ns-0 fLwyDF sc-1tbyx6t-1 kCGMTY iklhnl-0 eEewhk'}).getText()
            quantity = float(entries[0].getText().split(' ')[0])

        from_addr_with_quantities.append({'from_addr': from_addr, 'quantity': quantity})

    # Process the to_addresses
    to_addr_with_quantities = []
    for to_addr_div in to_addr_divs:
        entries = to_addr_div.findAll('span', {'class': 'sc-1ryi78w-0 cILyoi sc-16b9dsl-1 ZwupP u3ufsr-0 eQTRKC'})
        if len(entries) == 2:
            # If this is the current address (and so there's no link)
            to_addr = current_addr
            quantity = float(entries[1].getText().split(' ')[0])
        else:
            # If this is a different address (and so it's a link)
            to_addr = to_addr_div.find('a',
                                       {'class': 'sc-1r996ns-0 fLwyDF sc-1tbyx6t-1 kCGMTY iklhnl-0 eEewhk'}).getText()
            quantity = float(entries[0].getText().split(' ')[0])

        to_addr_with_quantities.append({'to_addr': to_addr, 'quantity': quantity})

    # 3 possible cases of mappings of from_addresses to to_addresses
    if not from_addr_with_quantities or not to_addr_with_quantities:
        # If no addresses
        tx_payments = []
    elif len(from_addr_with_quantities) == 1:
        # If one-to-many
        tx_payments = [Payment(from_addr_with_quantities[0]['from_addr'], to_addr_with_quantity['to_addr'],
                               to_addr_with_quantity['quantity'], crypto, timestamp) for to_addr_with_quantity in
                       to_addr_with_quantities]
    else:
        # If many-to-one
        tx_payments = [Payment(from_addr_with_quantity['from_addr'], to_addr_with_quantities[0]['to_addr'],
                               from_addr_with_quantity['quantity'], crypto, timestamp) for from_addr_with_quantity
                       in from_addr_with_quantities]

    # Tell BFS which addresses to add to the queue to crawl later
    connected_addresses = [list(addr.values())[0] for addr in from_addr_with_quantities + to_addr_with_quantities if
                           list(addr.values())[0] != current_addr]

    volume = sum([tx_payment.volume for tx_payment in tx_payments])
    # for tx_payment in tx_payments:
    #     print(tx_payment)

    # Fees are in row[2]; we discard this

    return tx_payments, connected_addresses, volume


def scrape_all_txns(html, current_addr, crypto):
    total_payments = []
    total_connected_addresses = []
    total_volume = 0
    total_n_txns = 0

    soup = BeautifulSoup(html, 'html.parser')
    txdivs = soup.findAll('div', {'class': 'sc-1fp9csv-0 koYsLf'})

    # Each txdiv is a <div> HTML fragment that contains exactly one txn
    for txdiv in txdivs:
        payments, connected_addresses, volume = scrape_one_txn(txdiv, current_addr, crypto)

        total_payments.extend(payments)
        total_connected_addresses.extend(connected_addresses)

        total_volume += volume
        total_n_txns += 1

    return total_payments, total_connected_addresses, total_volume, total_n_txns, len(total_payments)