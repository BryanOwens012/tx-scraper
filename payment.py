#!/usr/bin/env python3

'''
Stores the data associated with a payment
'''

from functools import total_ordering

# Decorator facilitates the comparison operators in case we decide to sort Payments in the future
@total_ordering
class Payment:
    '''
    Stores the data associated with a payment

    Attributes
    -----------
    from_addr : str
        The address that sent the payment (the payer)
    to_addr : str
        The address that received the payment (the payee)
    volume : float
        The number of crypto coins that were paid in this payment
        Crypto coins are divisible
    crypto : str
        The name of the crypto, e.g. 'BTC'
    timestamp : int
        The UNIX timestamp (in seconds since the epoch) when this payment happened

    Methods
    ----------
    (None)

    '''
    def __init__(self, from_addr, to_addr, volume = None, crypto = 'BTC', timestamp = 1):
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.volume = volume
        self.crypto = crypto.upper()
        self.timestamp = timestamp

    def __str__(self):
        # return f'{self.timestamp} ({self.quantity} {self.crypto}): {self.from_addr} --> {self.to_addr}'
        return self.__class__.__name__ + '(Epoch time ' + str(self.timestamp) + (" (" + str(self.volume) + " " + self.crypto + "): ").ljust(20) + self.from_addr + " --> " + self.to_addr + ")"

    def __eq__(self, other):
        return (self.from_addr, self.to_addr, self.volume, self.crypto, self.timestamp) == (other.from_addr, other.to_addr, other.volume, other.crypto, other.timestamp)

    def __lt__(self, other):
        return (self.crypto, self.timestamp, self.from_addr, self.to_addr, self.volume) < (other.crypto, other.timestamp, other.from_addr, other.to_addr, other.volume)

if __name__ == '__main__':
    params = ('1BZnKDdz4WH7vAzNQHP6CpGCVcj6kgye4g', '1EjT93BLiVRQUuwwsGqN6TfiwuMjDyqLQZ', 0.06200195, 'BTC', 1552404079000)
    p = Payment(*params)
    print(f'Sample payment: {p}\n\t(https://www.blockchain.com/btc/address/1BZnKDdz4WH7vAzNQHP6CpGCVcj6kgye4g)')