#!/usr/bin/env python3
'''
Breadth-first search: for every address scraped, queue up the addresses's counterparties for scraping
The payments data for each address is stored in the address's Address.payments (List), to be exported later

An address is like a user ID, e.g. '38p77ZLLHTzEd8TdMRttteD6DuFmxkyhY3'
An address in involved in many payments
Payment (address <--> address): one-to-one

A transaction contains many payments
Transaction (address <--> address): one-to-one, one-to-many, or many-to-one
'''


from graph import Graph
from address import Address
import queue
import datetime

class BFS:
    '''
    A builder class that contains all the state for a scraping session and keeps running in a BFS fashion

    Attributes
    -----------
    start_addr : str
        The address to start scraping at
    crypto : str
        The abbreviation of the crypto, e.g. 'BTC' for Bitcoin
    dir : str
        The name of the directory in which we store the downloaded pages (HTML) of the scraped addresses
    maxdepth : int
        The max depth of the BFS tree we allow before we terminate
    maxtime : int
        The max number of seconds that we allow this BFS to run
    maxsize : int
        The max size of our queue, to limit memory usage
    refresh : bool
        Whether to refresh the HTML pages that we already downloaded in previous runs
   debug: bool
        Whether to print debug messages to stdout
    g : Graph
        The graph that contains info on which addresses are connected to which others
    q : Queue(tuple([str, int]))
        The BFS queue (the queue of addresses to scrape in the future)
        Each elt of the Queue is a tuple([the address, the BFS tree depth in which the address will be scraped])

    Methods
    ----------
    set_maxdepth : int --> None
        Public setter
    set_maxtime : int --> None
        Public setter
    set_maxsize : int --> None
        Public setter
    set_refresh : bool --> None
        Public setter
    run : None --> None
        Runs the BFS until either maxdepth or maxtime is reached
    log : str --> None
        Prints debug messages if self.debug is True
    to_csv : None --> str
        Exports the BFS's g (Graph) and returns the output file path
    '''

    def __init__(self, crypto = 'BTC', start_addr = None, dir = 'addresses'):
        self.start_addr = start_addr
        self.crypto = crypto
        self.dir = dir

        self.maxdepth = 2
        self.maxtime = None
        self.maxsize = 0
        self.refresh = False
        self.debug = False

        self.g = Graph()
        self.q = queue.Queue(self.maxsize)

    def set_maxdepth(self, maxdepth):
        self.maxdepth = maxdepth

    def set_maxtime(self, maxtime):
        self.maxtime = datetime.timedelta(seconds = maxtime)

    def set_maxsize(self, maxsize = 0):
        self.maxsize = maxsize if maxsize else 0

    def set_refresh(self, refresh):
        self.refresh = refresh

    def run(self):
        # Start the BFS
        # Keeps running until the queue of addresses to scrape is empty

        if self.start_addr is None:
            self.log(f'{self.__class__.__name__} requires a start address; add one with bfs.set_start(start_addr).')
            return
        self.q.put(tuple([self.start_addr, 0]))
        begin_time = datetime.datetime.now()

        while not self.q.empty(): # while q is not empty
            if self.maxtime and datetime.datetime.now() - begin_time > self.maxtime:
                print(f'Time limit of {self.maxtime} has been reached. Cleaning up ...')
                return
            try:
                a_addr, depth = self.q.get()
                if depth > self.maxdepth:
                    return

                a = Address(a_addr, self.crypto, self.dir)
                a.download(self.refresh)
                a.scrape_address_txns()
                a.make_address_profile()

                self.g.update_address_with_object(a)

                for b_addr in a.connected_addresses:
                    if b_addr in self.g:
                        continue
                    self.log(b_addr)
                    self.q.put(tuple([b_addr, depth+1]))
                    self.g.add_address(b_addr)

            except:
                self.log('Encountered an error with an address. Skipping...')

    def log(self, content):
        if self.debug:
            print(content)

    def to_csv(self):
        return self.g.to_csv()