#!/usr/bin/env python3

'''
Misc utilities to process CSVs
'''

import pandas as pd
import os

class CsvUtils:
    '''
    Misc utilities to process CSVs

    Attributes
    ----------
    (None)

    Methods
    ----------
    @staticmethod merge : str, str, str, bool, bool, bool, bool --> pd.DataFrame
        Merges two DataFrames together on a certain column
    @statietmhod latest_csv_path : str, str --> str
        Returns the name of the most recent CSV file that starts with
        a certain prefix and that has its timestamp in its name
    @staticmethod keep_only_latest_csv_and_rename : str, str --> str
        Renames the most recent CSV file to the prefix, and deletes all less recent CSV files that have the prefix
        Returns the path of the newly renamed CSV file
    @staticmethod crop_by_timestamp_range : str, int, int --> None
        Truncates the CSV file by row timestamp, and exports the truncated CSV file
    @staticmethod truncuate_to : str, int, int --> None
        Truncates the CSV file by row number, and exports the truncated CSV file
    '''


    def __init__(self):
        pass

    @staticmethod
    def merge(path1, path2, on, drop_cols1 = None, rename_cols1 = None, drop_cols2 = None, rename_cols2 = None):
        df1 = pd.read_csv(path1)
        df2 = pd.read_csv(path2)
        if drop_cols1:
            df1.drop(columns = drop_cols1, inplace = True)
        if drop_cols2:
            df2.drop(columns = drop_cols2, inplace = True)
        if rename_cols1:
            df1.rename(columns = rename_cols1, inplace = True)
        if rename_cols2:
            df2.rename(columns = rename_cols2, inplace = True)
        return df1.merge(df2, how = 'inner', on = on).reset_index().drop(columns = ['index', 'Unnamed: 0_x', 'Unnamed: 0_y'])

    @staticmethod
    def latest_csv_path(prefix, dir = '.'):
        timestamps = [file.split(prefix)[1].split('.csv')[0] for file in os.listdir(dir) if file.startswith(prefix)]
        if not timestamps:
            return None
        return os.path.join(dir, prefix + max(timestamps) + '.csv')

    @staticmethod
    def keep_only_latest_csv_and_rename(prefix, dir = '.'):
        kept_path = CsvUtils.latest_csv_path(prefix, dir)
        others = [file for file in os.listdir(dir) if file.startswith(prefix)]
        delete_paths = [os.path.join(file) for file in others if file != os.path.basename(kept_path)]
        for path in delete_paths:
            os.remove(path)
        new_path = os.path.join(dir, prefix[:-1] + '.csv')
        os.rename(kept_path, new_path)
        return new_path

    @staticmethod
    def crop_by_timestamp_range(path, timestamp_begin, timestamp_end):
        if timestamp_begin > timestamp_end:
            return
        df = pd.read_csv(path)
        df = df[df['timestamp'] >= timestamp_begin]
        df = df[df['timestamp'] <= timestamp_end]
        if 'Unnamed: 0' in df.columns:
            df.drop(columns = 'Unnamed: 0', inplace = True)
        df.to_csv(path)

    @staticmethod
    def truncate_to(path, start = 0, stop = 100):
        if start > stop:
            return
        df = pd.read_csv(path)
        df = df.truncate(before = start, after = stop)
        if 'Unnamed: 0' in df.columns:
            df.drop(columns = 'Unnamed: 0', inplace = True)
        df.to_csv(path)
