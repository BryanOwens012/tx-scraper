#!/usr/bin/env python3

'''
The driver program
(1) Sets the program args according to runner_config.py
(2) Calls the BFS object to do the scraping
(3) Calls BFS.to_csv() to export a CSV containing the payments
(4) Calls CsvUtils.* to apply program args to the CSV (like truncation) and export the final CSV
'''

import os

from bfs import BFS
from csv_utils import CsvUtils

def run():
    '''
    The driver program

    Params: (None)
    Returns: (None)
    '''


    # Default configs
    origin_address = '38p77ZLLHTzEd8TdMRttteD6DuFmxkyhY3'
    addresses_dir = 'addresses'
    maxtime = 3  # in seconds
    maxdepth = 3
    refresh = False  # refresh data if already downloaded (cached) in the addresses_dir directory
    timestamp_range, row_range = None, None

    # Import configs from ./runner_config.py
    try:
        import runner_config
        origin_address = runner_config.origin_address
        addresses_dir = runner_config.addresses_dir
        maxtime = runner_config.maxtime
        maxdepth = runner_config.maxdepth
        maxsize = runner_config.maxsize
        refresh = runner_config.refresh
        timestamp_range = runner_config.timestamp_range
        row_range = runner_config.row_range
    except:
        pass

    # Run the BFS
    crypto = 'btc'
    if not os.path.exists(addresses_dir):
        os.mkdir(addresses_dir)
    bfs = BFS('BTC', origin_address, addresses_dir)
    bfs.set_maxdepth(maxdepth)
    bfs.set_maxtime(maxtime) # in terms of seconds
    bfs.set_maxsize(maxsize)
    bfs.set_refresh(refresh)
    bfs.run()
    out_path = bfs.to_csv()
    print(f'Wrote {out_path}', end = ' ')

    # If we want to discard old versions
    p = CsvUtils.keep_only_latest_csv_and_rename(prefix = 'payments-')
    print(f'and renamed it to {p}')

    # If we want to truncate by timestamp
    if timestamp_range is not None:
        try:
            # CsvUtils.crop_by_timestamp_range('payments.csv', 1504981858, 1513848888)
            CsvUtils.crop_by_timestamp_range(p, timestamp_range[0], timestamp_range[1])
            print(f'Truncated to timestamp range [{timestamp_range[0]}, {timestamp_range[1]}]')
        except:
            print('Could not truncate by timestamp')

    # If we want to truncate by row
    if row_range is not None:
        try:
            # CsvUtils.truncate_to(p, start = 0, stop = 100)
            CsvUtils.truncate_to(p, start = row_range[0], stop = row_range[1])
            print(f'Truncated to rows [{row_range[0]}, {row_range[1]}]')
        except:
            print('Could not truncate by row')

if __name__ == '__main__':
    run()