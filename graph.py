#!/usr/bin/env python3

'''
Stores the Address objects
Each Address object stores the list of payments associated with the address
'''


import pandas as pd
import datetime

class Graph:
    '''
    Is essentially a wrapper for a dictionary

    Attributes
    -----------
    bidirectional_adj_list : Dict(str => Address)
        I called this "bidirectional" because indexing in gives you access
        to both the senders and recipients of the address's payments through the Address object

    Methods
    ----------
    add_address : str --> None
        Creates a dict entry in bidirectional_adj_list so we know if the address was already
        added to the queue previously, even if it hasn't been scraped yet, to prevent redundant scraping
    update_address_with_object : Address --> None
        Maps the address's dict entry in bidirectional_adj_list to its Address object
    _format_payments : None --> pd.DataFrame
        Export bidirectional_adj_list to a DataFrame to later export to CSV
    to_csv : None --> str
        Exports the payments DataFrame to CSV, and returns the path of that CSV
    '''

    def __init__(self):
        self.bidirectional_adj_list = dict()
        # self.adj_list = None

    def __contains__(self, item):
        return item in self.bidirectional_adj_list

    def __str__(self):
        s = 'Graph({\n'
        for a, addr in self.bidirectional_adj_list.items():
            s += '\t' + a + ': ' + str(addr) + ',\n'
        s += '})'
        return s

    def add_address(self, addr):
        if addr not in self.bidirectional_adj_list:
            self.bidirectional_adj_list[addr] = None

    def update_address_with_object(self, address):
        self.bidirectional_adj_list[address.address] = address

    def _format_payments(self):
        count = 0
        df = pd.DataFrame(columns = ['timestamp', 'crypto', 'volume', 'from_address', 'to_address'])

        for addr_name, addr in self.bidirectional_adj_list.items():
            if addr is None:
                continue
            for payment in addr.payments:
                df.loc[count, 'timestamp'] = payment.timestamp
                df.loc[count, 'crypto'] = payment.crypto
                df.loc[count, 'volume'] = payment.volume
                df.loc[count, 'from_address'] = payment.from_addr
                df.loc[count, 'to_address'] = payment.to_addr
                count += 1
        return df

    def to_csv(self):
        # self.make_adj_list(True if format == 'payments' else False)
        out_path = f'payments-{int(round(datetime.datetime.timestamp(datetime.datetime.now()), 1))}.csv'

        self._format_payments().to_csv(out_path)

        # If memory-constrained, then follow these steps after each export:
        # (1) Make to_csv() append to the database instead of overwriting it
        # (2) Delete the key for each addr_name, such that
        # self.bidirectional_adj_list = {'addr0': None, 'addr1': None, ...}
        # Since we've already exported those addrs' data, we only need to keep the addr_names around to
        # let us know that we've already visited these addrs

        return out_path
