#!/usr/bin/env python3

'''
Stores the summary data associated with each address
'''

from functools import total_ordering

# Decorator facilitates the comparison operators in case we decide to sort AddressProfiles in the future
@total_ordering
class AddressProfile:
    '''
    Stores the summary of the address

    Attributes
    ----------
    address : str
        The address
    crypto : str
        The abbreviation of the crypto, e.g. 'BTC' for Bitcoin
    hash160 : str
        A hash of the address that Blockchain.com provides
    num_transactions : int
        The total number of transactions the address is involved in
    self.total_received : float
        The total number of crypto coins the address received
        Crypto coins are divisible
    self.total_sent : float
        The total number of crypto coins the address received
        Crypto coins are divisible
    self.final_balance : float
        The total number of crypto coins the address has remaining

    Methods
    -------
    (None)

    '''

    def __init__(self, address, crypto = 'BTC'):
        self.address = address
        self.crypto = crypto.upper()

        self.hash160 = None
        self.num_transactions = 0
        self.total_received = 0
        self.total_sent = 0
        self.final_balance = 0

    def __str__(self):
        return self.__class__.__name__ + '(' + self.address + ' (' + self.crypto + '))'

    def __eq__(self, other):
        return (self.crypto, self.address) == (other.crypto, other.address)

    def __lt__(self, other):
        return (self.crypto, self.address) < (other.crypto, other.address)