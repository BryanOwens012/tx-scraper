# tx-scraper

By Bryan Owens. Code originally written Mar 2019, updated Feb 2021.

## Summary

This program scrapes Blockchain.com for Bitcoin payment data. I developed this program as an intern at [CertiK](https://www.certik.com/), a blockchain security unicorn in NYC. At the time (March 2019), I was a college sophomore, and CertiK was seed-stage.

As my project didn't involve proprietary components and was intended as a proof-of-concept, I kept this IP.

Note that this project no longer runs properly as of Feb 2021, because Blockchain.com has made HTML DOM changes that conflict with my code.

## Motivation for this project

One major feature of blockchain that attracts criminals is its anonymity. Addresses (i.e., user IDs) are anonymous, anyone can use an unlimited number of addresses, and transactions aren't constrained by borders. Thus, criminals can secretly and strategically launder money around numerous accounts and between countries. Bitcoin sees $10B+ of transactions per day, and who knows how much are by criminals?

... We might know! Since all payments on the blockchain are necessarily public, we can keep track of all payments and try to correlate them with real-world events. Specifically, we can monitor for unusual transactions (such as large payments, many payments, or frequent payments between the same two addresses) and see if anything shows up on AML (anti-money laundering) databases, social media activity, and government data sources. Any patterns we find could help investigators unmask bad actors.

As CertiK was searching for product-market fit at the time, they asked me and a fellow intern to build a proof-of-concept for a blockchain-monitoring product. Our proof-of-concept contained two parts: (1) a web scraper to download blockchain transaction data into a database, (2) a JavaScript visualizer (frontend) to view the transactions as a graph network, and (3) a statistical analyzer to determine anomalous transactions or addresses. I took part (1), and my fellow intern took parts (2) and (3).

## Fairly nontechnical explanation of blockchain transactions (ignore if you're already familiar with blockchain)

A blockchain is essentially a distributed, public ledger (linked list) of transactions. As the ledger is distributed, every node (computer) in the blockchain network keeps its own copy of the ledger.

The nodes collectively agree on what the ledger should look like at any given moment through a "consensus mechanism": when a node wants to append a transaction to the ledger, the node solves a difficult cryptographic puzzle to justify that it should have the privilege to append that specific transaction. Everybody else then checks that node's work. Once a high enough proportion of nodes agree, everybody else automatically agrees too, and the transaction gets appended to the ledger. It takes on average 1 hr for the Bitcoin blockchain to append any particular transaction this way, but since transactions happen all the time, they are continuously appended to the ledger as they are agreed upon.

We can observe these transactions getting appended to the ledger by watching Blockchain.com. Each blockchain address (i.e., user ID) has its own page on Blockchain.com, where we can see which transactions the address participated in. For example, see the page for [address 38p77ZLLHTzEd8TdMRttteD6DuFmxkyhY3](https://www.blockchain.com/btc/address/38p77ZLLHTzEd8TdMRttteD6DuFmxkyhY3).

Note that a transaction is a container for multiple payments. For example, if Alice pays Bob 1 BTC, and Charlie pays Bob 2 BTC, then that's 2 payments totalling 3 BTC, but they can be processed together as 1 transaction totalling 3 BTC. Although a payment (address <--> address) is always one-to-one, a transaction (address <--> address) can be one-to-one, one-to-many, or many-to-one.

## Result

We took 1 week out of our 2-week spring break internship to code up the project. The company was so impressed with our proof-of-concept that they recognized its viability and later started developing their blockchain-monitoring platform. As of Feb 2021, that platform is called [CertiK Skynet](https://skynet.certik.com/skynet-101/skynet-security-score).

Here's an example of a network graph from March 2019:

![Bitcoin transactions network graph "peacock"](https://gitlab.com/BryanOwens012/tx-scraper/-/raw/master/screens/peacock-2000btc.png)

## System design

The system design looks something like this:

![Current system design](https://gitlab.com/BryanOwens012/tx-scraper/-/raw/master/screens/Current%20system%20design.png)

## Command-line output

A sample run of `runner.py` produces command-line output that looks something like this:

![Sample run of runner.py](https://gitlab.com/BryanOwens012/tx-scraper/-/raw/master/screens/Terminal%20sample%20output%202019-03-15%2013.09.23.png)

## Code breakdown (open the individual files for documentation)

### `README.md`

This file

### `runner_config.py`

The config file that the user can edit, with params such as the origin address (the first address our program scrapes) and maxtime (the program's time limit before it terminates).

### `runner.py`

The driver program, which runs the scraper and manipulates the output CSV file. Run **`$ python3 runner.py` to get this program going**.

### `bfs.py`

The class, called by `runner.py`, that performs breadth-first search to scrape all the addresses that have transacted with a certain address, and then scrape all the addresses that have transacted with those addresses, etc.

### `address.py`

The class that contains all the data pertinent to a given address, including a list of the payments that were involved with the address

### `graph.py`

The class that maps addresses to their Address objects, thereby serving as a container for all the Address objects and also enabling BFS to determine if an address was already visited.

### `payment.py`

The class that contains all the data pertinent to a given payment, such as the payer, payee, and quantity of the payment.

### `address_profile.py`

The class that contains summary data for a given address, such as the total number of transactions the address was involved in.

### `scraper.py`

The program that uses BeautifulSoup to scrape a Blockchain.com page's DOM for the transaction data.

### `csv.utils.py`

Misc utilities to handle CSVs, such as truncation

### `payments.csv`

The output CSV file containing all the payments data

### `addresses/`

The directory serving as a cache for the HTML files (the Blockchain.com pages) we downloaded, so we don't have to re-download them next time.

### `screens/`

Various screenshots of prior runs from March 2019

